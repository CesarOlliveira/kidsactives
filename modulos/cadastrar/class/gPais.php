<?php

include '../../app/PDOHandler.php';

class gPais {

    var $con;

    public function __CONSTRUCT() {
        $this->con = new PDOHandler();
    }

    public function adicionarNovosPais($idMaster, $idSituacaoUsuario, $nomePais, $generoPais, $cpfPais, $dataNascimentoPais, $emailPais, $senhaFinalPais) {
        $this->con->query("INSERT INTO tb_pais (id_master, id_situacao_usuario, nome_pais, genero_pais, cpf_pais, data_nasc_pais, email_pais, senha_pais) VALUES (" . $idMaster . ", " . $idSituacaoUsuario . ", '" . $nomePais . "', '" . $generoPais . "', '" . $cpfPais . "', '" . $dataNascimentoPais . "', '" . $emailPais . "', '" . $senhaFinalPais . "')");
        $this->con->execute();
    }

    public function carregaSituacoesPais() {
        try {
            $this->con->query("SELECT tb_situacao_usuario.id_situacao_usuario,tb_situacao_usuario.situacao_usuario FROM tb_situacao_usuario");
            $this->con->execute();

            $rst = $this->con->result_set();

            return $rst;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

}
?>

