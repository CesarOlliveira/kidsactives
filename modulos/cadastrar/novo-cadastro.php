<?php ?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Cadastrar-se</title>

        <!-- Custom fonts for this template-->
        <link href="../app/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">

    </head>
    <body class="bg-gradient-primary">
        <div class="container">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                        <div class="col-lg-7">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Cadastre-se</h1>
                                </div>
                                <form action="add/adiciona-pais.php" method="POST">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>Nome:</label>
                                                <input type="text" name="nomePais" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Gênero:</label>
                                                <select name="generoPais" class="form-control">
                                                    <option selected="true" disabled>Selecione...</option>
                                                    <option value="homem">Homem</option>
                                                    <option value="mulher">Mulher</option>
                                                    <option value="outros">Outros</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label>CPF:</label>
                                                <input type="text" name="cpfPais" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="form-group">
                                                <label>Data de Nascimento:</label>
                                                <input type="date" name="dataNascimentoPais" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label>E-mail:</label>
                                                <input type="email" name="emailPais" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Senha:</label>
                                                <input type="password" name="senhaPais1" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label>Repetir Senha:</label>
                                                <input type="password" name="senhaPais2" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">CADASTRAR</button>
                                    <hr>
                                </form>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Esqueceu sua senha?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="../login.php">Já é cadastrado? Ok, vamos Logar!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="../app/jquery/jquery.min.js"></script>
        <script src="../app/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../app/jquery-easing/jquery.easing.min.js"></script>
    </body>
</html>
