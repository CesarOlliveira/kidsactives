<?php

include '../class/gPais.php';

$novoCadastro = new gPais;

if (isset($_POST)) {
    $idMaster = 'NULL';
    $idSituacaoUsuario = 'NULL';
    $nomePais = $_POST[nomePais];
    $generoPais = $_POST[generoPais];
    $cpfPais = $_POST[cpfPais];
    $dataNascimentoPais = $_POST[dataNascimentoPais];
    $emailPais = $_POST[emailPais];
    $senhaPais1 = $_POST[senhaPais1];
    $senhaPais2 = $_POST[senhaPais2];

    // VERIFICA SENHA / CRIPTOGRAFA SENHA
    if ($senhaPais1 === $senhaPais2) {
        $senhaFinalPais = $senhaPais1;
    } else {
        die('Senha diferentes');
    }
    $senhaFinalPais = md5($senhaFinalPais);
} else {
    die('Ocorreu um erro ao receber os dados dos campos do formulário.');
}

$novoCadastro->adicionarNovosPais($idMaster, $idSituacaoUsuario, $nomePais, $generoPais, $cpfPais, $dataNascimentoPais, $emailPais, $senhaFinalPais);

?>