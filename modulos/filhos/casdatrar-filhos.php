<?php ?>

<!DOCTYPE html>
<html lang="pt-br">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Cadastrar Filhos</title>

        <!-- Custom fonts for this template-->
        <link href="../app/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">

    </head>
    <body id="page-top">

        <!-- INICIO DO BORY -->
        <div id="wrapper">

            <!-- MENU LATERAL -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <i class="fas fa-laugh-wink"></i>
                    </div>
                    <div class="sidebar-brand-text mx-3">Kids Activites</div>
                </a>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Painel de Controle</span></a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    GERENCIAR ATIVIDADES
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="cadastrar-atividades.php" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-user"></i>
                        <span>Cadastrar Atividades</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-check"></i>
                        <span>Validar Atividades</span>
                    </a>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    GERENCIAR FILHOS
                </div>

                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-user"></i>
                        <span>Filhos</span>
                    </a>
                </li>
                <!-- Nav Item - Pages Collapse Menu -->
                <li class="nav-item">
                    <a class="nav-link collapsed" href="casdatrar-filhos.php" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="fas fa-fw fa-user"></i>
                        <span>Cadastrar Filhos</span>
                    </a>
                </li>
            </ul>
            <!-- FINAL DO MENU LATERAL -->

            <!-- INICIO DA JANELA -->
            <div id="content-wrapper" class="d-flex flex-column">
                
                <!-- INICIO DA PAGINA -->
                <div id="content">
                    <div class="container-fluid">
                        <br>
                        <br>
                        <h1 class="text-center">CADASTRAR FILHOS</h1>
                        <br>
                        <br>
                        <form action="add/adiciona-filhos.php" method="POST">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Nome do filho:</label>
                                            <input type="text" name="nomeFilho" class="form-control" placeholder="Ex: Melissa Manoelly">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>Sexo:</label>
                                            <select name="sexoFilho" class="form-control">
                                                <option value="0" selected="true" disabled>Selecione uma opção</option>
                                                <option value="menino">Menino</option>
                                                <option value="menina">Menina</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>Idade:</label>
                                            <input type="number" name="idadeFilho" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label>Data de Nascimento:</label>
                                            <input type="date" name="dataNascimentoFilho" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Login do Filho:</label>
                                            <input type="text" name="login1" class="form-control" placeholder="Ex: MelissaManoelly">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Confirmar Login do Filho:</label>
                                            <input type="text" name="login2" class="form-control" placeholder="Ex: MelissaManoelly">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Senha:</label>
                                            <input type="password" name="senha1" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>Confirmar Senha:</label>
                                            <input type="password" name="senha2" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <button type="reset" class="btn btn-primary btn-sm btn-block">Limpar</button>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-sm btn-block">Salvar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- FINAL DA PAGINA -->

                <!-- INICIO DO FOOTER -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Desenvolvido por Coodifica. | Copyright © 2019 - Todos direitos reservados.</span>
                        </div>
                    </div>
                </footer>
                <!-- FINAL DO FOOTER -->
                
            </div>
            <!-- FINAL DA JANELA -->
            
        </div>
        <!-- FINAL DO BORY -->

        <!-- Bootstrap core JavaScript-->
        <script src="../app/jquery/jquery.min.js"></script>
        <script src="../app/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="../app/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src=""></script>
    </body>
</html>
