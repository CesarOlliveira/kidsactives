<?php

include '../class/gFilhos.php';

if (isset($_POST)) {
    $idMaster = 'NULL';
    $idSituacaoUsuario = 'NULL';
    $nomeFilho = $_POST[nomeFilho];
    $sexoFilho = $_POST[sexoFilho];
    $idadeFilho = $_POST[idadeFilho];
    $dataNascimentoFilho = $_POST[dataNascimentoFilho];
    $login1 = $_POST[login1];
    $login2 = $_POST[login2];
    $senha1 = $_POST[senha1];
    $senha2 = $_POST[senha2];

    // VALIDACAO LOGIN
    if ($login1 === $login2) {
        $loginFinalFilho = $login1;
    } else {
        die('Os campos de Login são diferentes. Por favor verifique e ajuste para continuar.');
    }

    // VALIDACAO SENHA
    if ($senha1 === $senha2) {
        $senhaFinalFilho = $senha1;
    } else {
        die('Os campos de senha são diferentes. Por favor verifique e ajuste para continuar.');
    }
    
    // CRIPTOGRAFANDO SENHA
    $senhaFinalFilho = md5($senhaFinalFilho);
    
} else {
    echo 'Ocorreu um erro ao receber os dados dos campos.';
}

$gFilho = new gFilhos;

$gFilho->adicionarFilhos($idMaster, $idSituacaoUsuario, $nomeFilho, $sexoFilho, $idadeFilho, $dataNascimentoFilho, $loginFinalFilho, $senhaFinalFilho);

?>
