-- phpMyAdmin SQL Dump
-- version 4.4.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 18-Nov-2019 às 10:50
-- Versão do servidor: 5.6.24
-- PHP Version: 5.6.10RC1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_kids_activites`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atividade`
--

CREATE TABLE IF NOT EXISTS `tb_atividade` (
  `id_atividade` int(11) NOT NULL,
  `id_master` int(11) DEFAULT NULL,
  `id_situacao_atividade` int(11) DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `id_filhos` int(11) DEFAULT NULL,
  `titulo_atividade` varchar(255) DEFAULT NULL,
  `descricao_atividade` longtext,
  `valor_atividade` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_filhos`
--

CREATE TABLE IF NOT EXISTS `tb_filhos` (
  `id_filhos` int(11) NOT NULL,
  `id_master` int(11) DEFAULT NULL,
  `id_situacao_usuario` int(11) DEFAULT NULL,
  `nome_filhos` varchar(255) DEFAULT NULL,
  `genero_filhos` varchar(255) DEFAULT NULL,
  `idade_filhos` varchar(255) DEFAULT NULL,
  `data_nasc_filhos` date DEFAULT NULL,
  `login_filhos` varchar(255) DEFAULT NULL,
  `senha_filhos` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_filhos`
--

INSERT INTO `tb_filhos` (`id_filhos`, `id_master`, `id_situacao_usuario`, `nome_filhos`, `genero_filhos`, `idade_filhos`, `data_nasc_filhos`, `login_filhos`, `senha_filhos`) VALUES
(5, NULL, NULL, 'Pedro Pedrosa Bouer', 'menino', '0', '2019-03-15', NULL, NULL),
(7, NULL, NULL, 'Melissa Manoelly', 'menina', '2', '2017-11-25', 'MelissaManoelly', 'd2bf8ab81224a4b6547e4a3a951c87f1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_filiacao`
--

CREATE TABLE IF NOT EXISTS `tb_filiacao` (
  `id_filiacao` int(11) NOT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `id_filhos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_master`
--

CREATE TABLE IF NOT EXISTS `tb_master` (
  `id_master` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_pais`
--

CREATE TABLE IF NOT EXISTS `tb_pais` (
  `id_pais` int(11) NOT NULL,
  `id_master` int(11) DEFAULT NULL,
  `id_situacao_usuario` int(11) DEFAULT NULL,
  `nome_pais` varchar(255) DEFAULT NULL,
  `genero_pais` varchar(255) DEFAULT NULL,
  `cpf_pais` varchar(255) DEFAULT NULL,
  `data_nasc_pais` date DEFAULT NULL,
  `email_pais` varchar(255) DEFAULT NULL,
  `senha_pais` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_pais`
--

INSERT INTO `tb_pais` (`id_pais`, `id_master`, `id_situacao_usuario`, `nome_pais`, `genero_pais`, `cpf_pais`, `data_nasc_pais`, `email_pais`, `senha_pais`) VALUES
(2, NULL, NULL, 'Cesar de Oliveira Junior', 'homem', '057.101.841-60', '1997-09-17', 'cesarolliveira.jr@gmail.com', 'd2bf8ab81224a4b6547e4a3a951c87f1'),
(3, NULL, NULL, 'Jaqueline Azevedo Espirito Santo', 'mulher', '173.674.171-34', '1996-10-17', 'jaqueline.azevedo@gmail.com', '78cd37933a798362c10eb8d6189aaf22'),
(4, NULL, NULL, ' Heitor Henry da Cruz', 'homem', '276.715.011-40', '1993-01-27', 'heitorhenrydacruz-74@live.ca', '6b8eac60aacd64d83fa597affe997d56'),
(8, NULL, NULL, ' Fernanda Evelyn Aparecida da Paz', 'mulher', '886.327.881-40', '1990-08-02', 'fernandaevelyn@aerobravo.com.br', '03a91f25e8a90abd34ceeb4ad306209b'),
(20, NULL, NULL, ' José Marcos Vinicius Igor Pires', 'homem', '039.168.141-92', '1990-05-11', 'josemarcosviniciusigorpires@hotmail.com', '3517195ca8563df340cb094cdd6e3407'),
(22, NULL, NULL, ' Joana Vera Gomes', 'mulher', '349.971.121-43', '1987-02-14', 'joanaveragomes@graficajardim.com.br', '28526fdaf1bf0334e0f45634ebbfaae1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_situacao_atividade`
--

CREATE TABLE IF NOT EXISTS `tb_situacao_atividade` (
  `id_situacao_atividade` int(11) NOT NULL,
  `situacao_atividade` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_situacao_usuario`
--

CREATE TABLE IF NOT EXISTS `tb_situacao_usuario` (
  `id_situacao_usuario` int(11) NOT NULL,
  `situacao_usuario` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_atividade`
--
ALTER TABLE `tb_atividade`
  ADD PRIMARY KEY (`id_atividade`) USING BTREE,
  ADD KEY `id_master` (`id_master`) USING BTREE,
  ADD KEY `id_pais` (`id_pais`) USING BTREE,
  ADD KEY `id_filhos` (`id_filhos`) USING BTREE,
  ADD KEY `id_situacao_atividade` (`id_situacao_atividade`) USING BTREE;

--
-- Indexes for table `tb_filhos`
--
ALTER TABLE `tb_filhos`
  ADD PRIMARY KEY (`id_filhos`) USING BTREE,
  ADD KEY `id_situacao_usuario` (`id_situacao_usuario`) USING BTREE,
  ADD KEY `id_master` (`id_master`) USING BTREE;

--
-- Indexes for table `tb_filiacao`
--
ALTER TABLE `tb_filiacao`
  ADD PRIMARY KEY (`id_filiacao`) USING BTREE,
  ADD KEY `id_filhos` (`id_filhos`) USING BTREE,
  ADD KEY `id_pais` (`id_pais`) USING BTREE;

--
-- Indexes for table `tb_master`
--
ALTER TABLE `tb_master`
  ADD PRIMARY KEY (`id_master`) USING BTREE;

--
-- Indexes for table `tb_pais`
--
ALTER TABLE `tb_pais`
  ADD PRIMARY KEY (`id_pais`) USING BTREE,
  ADD KEY `id_master` (`id_master`) USING BTREE,
  ADD KEY `id_situacao_usuario` (`id_situacao_usuario`) USING BTREE;

--
-- Indexes for table `tb_situacao_atividade`
--
ALTER TABLE `tb_situacao_atividade`
  ADD PRIMARY KEY (`id_situacao_atividade`) USING BTREE;

--
-- Indexes for table `tb_situacao_usuario`
--
ALTER TABLE `tb_situacao_usuario`
  ADD PRIMARY KEY (`id_situacao_usuario`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_atividade`
--
ALTER TABLE `tb_atividade`
  MODIFY `id_atividade` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_filhos`
--
ALTER TABLE `tb_filhos`
  MODIFY `id_filhos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_filiacao`
--
ALTER TABLE `tb_filiacao`
  MODIFY `id_filiacao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_master`
--
ALTER TABLE `tb_master`
  MODIFY `id_master` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_pais`
--
ALTER TABLE `tb_pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tb_situacao_atividade`
--
ALTER TABLE `tb_situacao_atividade`
  MODIFY `id_situacao_atividade` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_situacao_usuario`
--
ALTER TABLE `tb_situacao_usuario`
  MODIFY `id_situacao_usuario` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tb_atividade`
--
ALTER TABLE `tb_atividade`
  ADD CONSTRAINT `tb_atividade_ibfk_1` FOREIGN KEY (`id_master`) REFERENCES `tb_master` (`id_master`),
  ADD CONSTRAINT `tb_atividade_ibfk_2` FOREIGN KEY (`id_pais`) REFERENCES `tb_pais` (`id_pais`),
  ADD CONSTRAINT `tb_atividade_ibfk_3` FOREIGN KEY (`id_filhos`) REFERENCES `tb_filhos` (`id_filhos`),
  ADD CONSTRAINT `tb_atividade_ibfk_4` FOREIGN KEY (`id_situacao_atividade`) REFERENCES `tb_situacao_atividade` (`id_situacao_atividade`);

--
-- Limitadores para a tabela `tb_filhos`
--
ALTER TABLE `tb_filhos`
  ADD CONSTRAINT `tb_filhos_ibfk_1` FOREIGN KEY (`id_situacao_usuario`) REFERENCES `tb_situacao_usuario` (`id_situacao_usuario`),
  ADD CONSTRAINT `tb_filhos_ibfk_2` FOREIGN KEY (`id_master`) REFERENCES `tb_master` (`id_master`);

--
-- Limitadores para a tabela `tb_filiacao`
--
ALTER TABLE `tb_filiacao`
  ADD CONSTRAINT `tb_filiacao_ibfk_1` FOREIGN KEY (`id_filhos`) REFERENCES `tb_filhos` (`id_filhos`),
  ADD CONSTRAINT `tb_filiacao_ibfk_2` FOREIGN KEY (`id_pais`) REFERENCES `tb_pais` (`id_pais`);

--
-- Limitadores para a tabela `tb_pais`
--
ALTER TABLE `tb_pais`
  ADD CONSTRAINT `tb_pais_ibfk_1` FOREIGN KEY (`id_master`) REFERENCES `tb_master` (`id_master`),
  ADD CONSTRAINT `tb_pais_ibfk_2` FOREIGN KEY (`id_situacao_usuario`) REFERENCES `tb_situacao_usuario` (`id_situacao_usuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
